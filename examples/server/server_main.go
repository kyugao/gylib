package main

import (
	"gitlab.com/kyugao/gylib/node"
	"gitlab.com/kyugao/gylib/node/services/echo"
)

func main() {
	node.InitServer()
	node.RegisterHandler(echo.Handler())
	node.ServerStartNode()
}
