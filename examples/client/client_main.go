package main

import (
	"time"
	"gitlab.com/kyugao/gylib/node"
	"gitlab.com/kyugao/gylib/logger"
	"gitlab.com/kyugao/gylib/node/services/echo"
)

func main() {
	client := node.InitClient("localhost:8090")
	go shutdown(client)
	LoopEcho(client)
}

func shutdown(client *node.Client) {
	time.Sleep(time.Second * 15)
	client.ShutDown = true
}

func LoopEcho(node *node.Client) {
	for {
		if node.ShutDown {
			logger.Infof("node has been shutdown.")
			break
		}
		echo.Echo(node)
		time.Sleep(time.Second * 3)
	}
}
