package db

import (
	"fmt"
	"strings"
	"gopkg.in/mgo.v2"
	"gitlab.com/kyugao/gylib/util/json"
	"log"
	"gopkg.in/mgo.v2/bson"
	"reflect"
)

type config struct {
	Hosts  []string
	User   string
	Pass   string
	DBName string
}

type Collection interface {
	ColName() string
}

func GetDBName() (dbName string) {
	if cfg == nil {
		return
	}
	dbName = cfg.DBName
	return
}

func (cfg *config) getUrl() (url string) {
	if cfg == nil {
		return
	}
	hosts := strings.Join(cfg.Hosts, ",")
	var credential string
	if len(cfg.User) > 0 && len(cfg.Pass) > 0 {
		credential = fmt.Sprint("%s:%s@", cfg.User, cfg.Pass)
	}
	url = fmt.Sprintf("mongodb://%s%s/%s", credential, hosts, cfg.DBName)
	log.Printf("解析数据库连接地址：%s.", url)
	return
}

var cfg *config
var session *mgo.Session

func init() {
	cfg = &config{}
	err := json.FromFile("./conf/mongodb.json", cfg)
	if err != nil {
		log.Printf("解析数据库配置文件错误 %v.", err)
		panic(err)
	}
	session, err = mgo.Dial(cfg.getUrl())
	if err != nil {
		log.Printf("连接数据库失败 %v.", err)
		panic(err)
	}
}

func GetDB() (*mgo.Database) {
	return session.DB(cfg.DBName)
}

func GetCollection(obj interface{}) (collection *mgo.Collection) {
	if temp, ok := obj.(Collection); ok {
		collection = GetDB().C(temp.ColName())
	} else {
		collection = nil
		log.Printf("assertion fail")
	}
	return
}

func GetDBRef(obj interface{}, id bson.ObjectId) (ref *mgo.DBRef) {
	if temp, ok := obj.(Collection); ok {
		log.Printf("test : %v, ok %v.", reflect.TypeOf(temp).String(), ok)
		ref = &mgo.DBRef{
			Collection: temp.ColName(),
			Id:         id,
			Database:   GetDBName(),
		}
	}
	return
}

func GetDBRefs(obj interface{}, ids []bson.ObjectId) (refs []*mgo.DBRef) {
	refs = make([]*mgo.DBRef, 0)
	var colName string
	if temp, ok := obj.(Collection); ok {
		colName = temp.ColName()
	} else {
		return
	}
	for _, id := range ids {
		ref := &mgo.DBRef{
			Collection: colName,
			Id:         id,
			Database:   GetDBName(),
		}
		refs = append(refs, ref)
	}
	return
}

func GetGridFS(category string) *mgo.GridFS {
	return GetDB().GridFS(category)
}
