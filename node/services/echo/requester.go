package echo

import (
	"golang.org/x/net/context"
	"gitlab.com/kyugao/gylib/logger"
	"gitlab.com/kyugao/gylib/util/json"
	"gitlab.com/kyugao/gylib/node/proto"
	"gitlab.com/kyugao/gylib/node/message"
	"gitlab.com/kyugao/gylib/node"
	"gitlab.com/kyugao/gylib/node/model"
)

type Response struct {
	model.BaseResp
}

func Echo(node *node.Client) {
	content := &Request{}
	key, err := message.CacheMsg(content)
	if err != nil {
		logger.Errorf("cache message content error %v.", err)
	}
	reqMsg := &proto.Message{
		Action: "echo",
		Key:    key,
	}
	respMsg, err := node.Call(context.Background(), reqMsg)
	if err != nil {
		logger.Errorf("call remote service error %v.", err)
		return
	}
	response := &Response{}
	err = message.GetMsg(respMsg.Key, response)
	if err != nil {
		logger.Errorf("load cached response error %v.", err)
	} else {
		logger.Debugf("echo response: %s.", json.ToString(response))
	}
}
