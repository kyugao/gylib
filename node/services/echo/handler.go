package echo

import (
	"gitlab.com/kyugao/gylib/node/message"
	"gitlab.com/kyugao/gylib/node/model"
)

type Request struct{}

type actionHandler struct {
	Action string
	Uri    string
}

var defaultHandler = &actionHandler{Action: "echo", Uri: "/node/echo"}

func Handler() (handler *actionHandler) {
	handler = defaultHandler
	return
}

func (h *actionHandler) GetAction() (action string) {
	action = h.Action
	return
}

func (h *actionHandler) GetUri() (uri string) {
	uri = h.Uri
	return
}

func (h *actionHandler) Handle(key string) (response interface{}, err error) {
	request := &Request{}
	err = message.GetMsg(key, request)
	response, err = echoProcessor()
	return
}

func echoProcessor() (response *Response, err error) {
	response = &Response{}
	response.SetRC(model.RC_SUCC)
	return
}
