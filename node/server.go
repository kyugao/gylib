package node

import (
	"net"
	"google.golang.org/grpc"
	"golang.org/x/net/context"
	"github.com/BurntSushi/toml"
	"gitlab.com/kyugao/gylib/logger"
	"gitlab.com/kyugao/gylib/node/proto"
	"gitlab.com/kyugao/gylib/util/json"
	"gitlab.com/kyugao/gylib/node/message"
)

type Config struct {
	Name    string
	Address string
}

type Handler interface {
	Handle(string) (interface{}, error)
	GetAction() (string)
	GetUri() (string)
}

func RegisterHandler(inHandler interface{}) {
	handler, ok := inHandler.(Handler)
	if ok {
		action := handler.GetAction()
		logger.Infof("Register handler action:%s.", action)
		node.Handlers[action] = handler
	} else {
		logger.Error("Register handler action: invalid input handler.")
	}
}

type Server struct {
	Name     string
	Listener net.Listener
	Handlers map[string]Handler
}

var node *Server
var config *Config

/*
 * 初始化单例Server对象
 */
func InitServer() {
	loadConfig()
	node = &Server{
		Name:     config.Name,
		Handlers: make(map[string]Handler),
	}
}

/*
 * 加载 Server 配置文件
 */
func loadConfig() {
	config = &Config{}
	_, err := toml.DecodeFile("./conf/server_node.conf", config)
	if err != nil {
		panic(err)
	} else {
		logger.Debug(json.ToString(config))
	}
}

func ServerStartNode() {
	lis, err := net.Listen("tcp", config.Address)
	if err != nil {
		panic(err)
	}
	node.Listener = lis
	logger.Infof("Start %s server node at %s.", config.Name, config.Address)
	grpcServer := grpc.NewServer()
	proto.RegisterNodeServer(grpcServer, node)
	grpcServer.Serve(node.Listener)
}

func (s *Server) Call(ctx context.Context, inMsg *proto.Message) (outMsg *proto.Message, err error) {
	outMsg = &proto.Message{}
	logger.Debugf("called %s.", json.ToString(inMsg))

	handler := s.Handlers[inMsg.Action]
	resp, err := handler.Handle(inMsg.Key)
	if err != nil {
		logger.Errorf("%s handler return error %v.", inMsg.Action, err)
		return
	}

	outKey, err := message.CacheMsg(resp)
	if err != nil {
		logger.Errorf("cache %s response message error %v.", inMsg.Action, err)
		return
	}

	outMsg.Action = inMsg.Action
	outMsg.Key = outKey
	return
}
