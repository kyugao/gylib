package node

import (
	"google.golang.org/grpc"
	"gitlab.com/kyugao/gylib/logger"
	"gitlab.com/kyugao/gylib/node/proto"
	"context"
)

type Client struct {
	protoClient proto.NodeClient
	ShutDown    bool
}

func InitClient(addr string) (client *Client) {
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	conn, err := grpc.Dial(addr, opts...)
	if err != nil {
		logger.Errorf("dial error %v.", err)
		panic(err)
	}
	client = &Client{
		protoClient: proto.NewNodeClient(conn),
		ShutDown:    false,
	}

	return
}

func (c *Client) Call(ctx context.Context, in *proto.Message, opts ...grpc.CallOption) (out *proto.Message, err error) {
	out, err = c.protoClient.Call(ctx, in, opts...)
	return
}
